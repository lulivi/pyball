# PyBall Manager

Telegram bot helper.

## Usage

First rename the [`sample.env`](./sample.env) file to `.env` and fill it in with
the proper values.

Then install the package with `poetry`:

```bash
pip3 install poetry==1.2.1
poetry install
```

And run the bot!

```bash
poetry run pyball
```

## License

This project is released under [MIT](./LICENSE) license.
